name: $PSVersionTable
keyword: 版本信息
desc: 打印PowerShell的版本信息
path: version
===

## $PSVersionTable

打印Powershell的版本信息

### 命令格式

```powershell
PS C:\ > $PSVersionTable
```

### 命令输出

```text
Name                           Value
----                           -----
PSVersion                      5.1.17763.1
PSEdition                      Desktop
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0...}
BuildVersion                   10.0.17763.1
CLRVersion                     4.0.30319.42000
WSManStackVersion              3.0
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1
```