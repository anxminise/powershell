name: help|man|Get-Help
keyword: 帮助文档
desc: 帮助查看命令的使用方式
path: help
===

## help/man/Get-Help

查看命令的使用方式

### 命令格式

```powershell
PS C:\ > help Get-Service
```

```powershell
PS C:\ > man Get-Service
```

```powershell
PS C:\ > Get-Help Get-Service
```

### 说明

1. help是一个函数，它自动将过多信息通过管道传给 `More`命令
2. man是help的一个别名
3. Get-Help是内置命令，它需手动将信息通过管道传给 `More`命令，如：`Get-Help Get-Content | More`
4. help支持模糊查询命令名称，如：`help *log*`将列出全部带有`log`字符串的命令

### 命令输出

```text
名称
    Get-Service

摘要
    Gets the services on a local or remote computer.


语法
    Get-Service [-ComputerName <String[]>] [-DependentServices] -DisplayName <String[]> [-Exclude <String[]>] [-Include
     <String[]>] [-RequiredServices] [<CommonParameters>]

    Get-Service [-ComputerName <String[]>] [-DependentServices] [-Exclude <String[]>] [-Include <String[]>] [-InputObje
    ct <ServiceController[]>] [-RequiredServices] [<CommonParameters>]

    Get-Service [[-Name] <String[]>] [-ComputerName <String[]>] [-DependentServices] [-Exclude <String[]>] [-Include <S
    tring[]>] [-RequiredServices] [<CommonParameters>]


说明
    The Get-Service cmdlet gets objects that represent the services on a local computer or on a remote computer, includ
    ing running and stopped services.

    You can direct this cmdlet to get only particular services by specifying the service name or display name of the se
    rvices, or you can pipe service objects to this cmdlet.


相关链接
    New-Service
    Restart-Service
    Resume-Service
    Set-Service
    Start-Service
    Stop-Service
    Suspend-Service

备注
    若要查看示例，请键入: "get-help Get-Service -examples".
    有关详细信息，请键入: "get-help Get-Service -detailed".
    若要获取技术信息，请键入: "get-help Get-Service -full".
    有关在线帮助，请键入: "get-help Get-Service -online"
```

### 其他相关命令

#### update-help

在线更新帮助文档信息

##### 命令格式

```powershell
PS C:\ > update-help
```
