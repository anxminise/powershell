name: get-alias
keyword: 昵称
desc: 查询命令的昵称
path: alias
===

## get-alias

查询获取命令的昵称

### 命令格式

```powershell
PS C:\ > get-alias -Definition "Get-Service"
```

### 命令输出

```text
CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Alias           gsv -> Get-Service
```