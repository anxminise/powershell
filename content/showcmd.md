name: Show-Command
keyword: GUI|显示|命令
desc: 图形界面的方式显示命令
path: showcmd
===

## Show-Command

以图形界面的方式显示命令

### 命令格式

```powershell
PS C:\ > Show-Command
```